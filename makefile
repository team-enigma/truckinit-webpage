IMAGE_NAME=webpage
IMAGE_TAG := $(shell date +%d-%m-%Y-T-%H-%M-%S)
export IMAGE_TAG
REGISTRY=registry.gitlab.com/team-enigma/truckinit-webpage

run:
	@npx serve

dockerimage:
	@docker build -t $(REGISTRY)/$(IMAGE_NAME):$(t) ./

pushimage:
	@docker push $(REGISTRY)/$(IMAGE_NAME):$(t)

deployimage:
	@bash -c "cd $$(pwd) && \
		make dockerimage t=$(IMAGE_TAG) && \
		make dockerimage t=latest && \
		make pushimage t=$(IMAGE_TAG) && \
		make pushimage t=latest"

deploy: deployimage
	@kubectl set image deployment truckinit-webpage-deployment \
		webpage-container=${REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG} --record
