FROM node:13.10.1-alpine3.10

WORKDIR /opt/website
COPY . ./
EXPOSE 5000

CMD ["npx", "serve"]
